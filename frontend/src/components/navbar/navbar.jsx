import React from "react";
import "../navbar/navbar.css";

const Navbar = () => {
	return (
		<div className="navbar-container">
			<a>Beranda</a>
			<a>Toko</a>
			<a>Tentang</a>
			<a>Blog</a>
		</div>
	);
};

export default Navbar;
